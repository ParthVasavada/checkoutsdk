

Pod::Spec.new do |s|

    # ―――  Spec Metadata  ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

    s.name         = "libMTPCommunicator"
    s.version      = "5.0.2"
    s.summary      = "MTP communicator library."

    s.description  = <<-DESC
                   * MTP communicator library.
                   DESC

    s.homepage     = "https://developer.mastercard.com"


    # ―――  Spec License  ――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

    s.license      = "Copyright 2014 C-SAM, A MasterCard Company. All rights reserved."
    # s.license      = { :type => "MIT", :file => "FILE_LICENSE" }


    # ――― Author Metadata  ――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

    s.author             = { "semal.gajera" => "semal.gajera@c-sam.com" }

    # ――― Source Location ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

        s.source = { :http => "http://artifactory.c-sam.com/artifactory/simple/mtp-ios-test/com/csam/directexpress/mtp/iOS/communicator/5.0.2.GA/communicator-5.0.2.GA.zip" }

    # ――― Source Code ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

    s.source_files  = "MTPCommunicatorHeaders/*.h"
    s.vendored_libraries = "libMTPCommunicator.a"

    #s.exclude_files = "Classes/Exclude"
    # s.public_header_files = "Classes/**/*.h"

    # ――― Xcode Project Configration ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

    s.xcconfig  =  { 'LIBRARY_SEARCH_PATHS' => '"$(PODS_ROOT)/libMTPCommunicator"', 'HEADER_SEARCH_PATHS' => '"${PODS_ROOT}/libMTPCommunicator/MTPCommunicatorHeaders"' }

end
