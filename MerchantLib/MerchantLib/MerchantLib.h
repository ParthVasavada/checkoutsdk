//
//  MerchantLib.h
//  MerchantLib
//
//  Created by Parth Vasavada on 8/19/15.
//  Copyright (c) 2015 MasterCard. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for MerchantLib.
FOUNDATION_EXPORT double MerchantLibVersionNumber;

//! Project version string for MerchantLib.
FOUNDATION_EXPORT const unsigned char MerchantLibVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <MerchantLib/PublicHeader.h>


