//
//  CoreSDK.h
//  CoreSDK
//
//  Created by Parth Vasavada on 8/20/15.
//  Copyright (c) 2015 MasterCard. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for CoreSDK.
FOUNDATION_EXPORT double CoreSDKVersionNumber;

//! Project version string for CoreSDK.
FOUNDATION_EXPORT const unsigned char CoreSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <CoreSDK/PublicHeader.h>


