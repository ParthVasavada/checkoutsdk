

Pod::Spec.new do |s|

    # ―――  Spec Metadata  ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

    s.name         = "MerchantLib"
    s.version      = "1.0.0"
    s.summary      = "Merchant Checkout SDK."

    s.description  = <<-DESC
                   * Merchant Checkout SDK.
                   DESC

    s.homepage     = "https://developer.mastercard.com"


    # ―――  Spec License  ――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

    s.license      = "Copyright 2015 MasterPass, A MasterCard Company. All rights reserved."
    # s.license      = { :type => "MIT", :file => "FILE_LICENSE" }


    # ――― Author Metadata  ――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

    s.author             = { "parth.vasavada" => "parth.vasavada" }

    # ――― Source Location ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

        s.source = { :http => "To be decided" }

    # ――― Source Code ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

    s.ios.vendored_frameworks = 'MerchantLib.framework'

    #s.exclude_files = "Classes/Exclude"
    # s.public_header_files = "Classes/**/*.h"

    # ――― Xcode Project Configration ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

    s.xcconfig  =  { 'FRAMEWORK_SEARCH_PATHS' => '"$(PODS_ROOT)/MerchantLib"', 'HEADER_SEARCH_PATHS' => '"${PODS_ROOT}/MerchantLib/MerchantLib.framework/Headers"' }

end
