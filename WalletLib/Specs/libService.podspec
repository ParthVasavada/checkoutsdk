
Pod::Spec.new do |s|

    # ―――  Spec Metadata  ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

    s.name         = "libService"
    s.version      = "5.1.0"
    s.summary      = "MTP service library."

    s.description  = <<-DESC
                    * MTP service library.
                    DESC

    s.homepage     = "https://developer.mastercard.com"

    # ―――  Spec License  ――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

    s.license      = "Copyright 2014 C-SAM, A MasterCard Company. All rights reserved."
    # s.license      = { :type => "MIT", :file => "FILE_LICENSE" }


    # ――― Author Metadata  ――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

    s.author             = { "semal.gajera" => "semal.gajera@c-sam.com" }

    # ――― Source Location ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

    s.source = { :http => "http://artifactory.c-sam.com/artifactory/simple/mtp-release-libs/com/csam/mtp/toolkits/enabling/iOS/service/5.1.0.GA/service-5.1.0.GA.zip" }

    # ――― Source Code ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

    s.source_files  = "ServiceHeaders/*.h"
    s.vendored_libraries = "libService.a"

    #s.exclude_files = "Classes/Exclude"
    # s.public_header_files = "Classes/**/*.h"

    # ――― Xcode Project Configration ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

    s.xcconfig  =  { 'LIBRARY_SEARCH_PATHS' => '"$(PODS_ROOT)/libService"', 'HEADER_SEARCH_PATHS' => '"${PODS_ROOT}/libService/ServiceHeaders"' }

end
