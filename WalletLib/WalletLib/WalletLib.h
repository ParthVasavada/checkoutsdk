//
//  WalletLib.h
//  WalletLib
//
//  Created by Parth Vasavada on 8/19/15.
//  Copyright (c) 2015 MasterCard. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for WalletLib.
FOUNDATION_EXPORT double WalletLibVersionNumber;

//! Project version string for WalletLib.
FOUNDATION_EXPORT const unsigned char WalletLibVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <WalletLib/PublicHeader.h>


