

Pod::Spec.new do |s|

    # ―――  Spec Metadata  ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

    s.name         = "MTPSerializationFramework"
    s.version      = "5.0.0"
    s.summary      = "MTP serialization framework."

    s.description  = <<-DESC
                   * MTP serialization framework.
                   DESC

    s.homepage     = "https://developer.mastercard.com"


    # ―――  Spec License  ――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

    s.license      = "Copyright 2014 C-SAM, A MasterCard Company. All rights reserved."
    # s.license      = { :type => "MIT", :file => "FILE_LICENSE" }


    # ――― Author Metadata  ――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

    s.author             = { "parth.vasavada" => "parth.vasavada@c-sam.com" }

    # ――― Source Location ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

        s.source = { :http => "http://artifactory.c-sam.com/artifactory/simple/mtp-release-libs/com/mastercard/mtp/toolkits/iOS/framework/serialization/5.0.0.GA/serialization-5.0.0.GA.zip" }

    # ――― Source Code ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

    s.ios.vendored_frameworks = 'Serialization.framework'

    #s.exclude_files = "Classes/Exclude"
    # s.public_header_files = "Classes/**/*.h"

    # ――― Xcode Project Configration ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

    s.xcconfig  =  { 'FRAMEWORK_SEARCH_PATHS' => '"$(PODS_ROOT)/MTPSerializationFramework"', 'HEADER_SEARCH_PATHS' => '"${PODS_ROOT}/MTPSerializationFramework/Serialization.framework/Headers"' }

end
